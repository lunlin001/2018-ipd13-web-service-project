﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace ServerBookingApp
{
    public class Customer
    {
        public int id { get; set; }

        [Required]
        public string CustFirstName { get; set; }

        [Required]
        public string CustLastName { get; set; }

        [Required]        
        public string CustEmail { get; set; }

        [Required]
        public string CustPhone { get; set; }
    }
}
