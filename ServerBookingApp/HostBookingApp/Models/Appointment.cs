﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServerBookingApp
{
    public class Appointment
    {
        public int id { get; set; }
        [Required]
        public Employee Employee { get; set; }
        public int EmployeeId { get; set; }

        [Required]
        public Customer Customer { get; set; }
        public int CustomerId { get; set; }

        [Required]
        public DateTime AptDate { get; set; }

        [Required]
        public DateTime AptTime { get; set; }
    }
}
