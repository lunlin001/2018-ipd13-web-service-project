﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ServerBookingApp
{
    public class Employee
    {
        public int id { get; set; }

        [Required]
        public string EmpFirstName { get; set; }

        [Required]
        public string EmpLastName { get; set; }

        [Required]        
        public string EmpEmail { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string EmpPhone { get; set; }

        [Required]
        public string Position { get; set; }

        [Required]
        public double Salary { get; set; }
    }
}
