namespace HostBookingApp.Migrations
{
    using ServerBookingApp;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ServerBookingApp.BookingDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ServerBookingApp.BookingDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            /*
            List<ServerBookingApp.Customer> customers = new List<Customer>();
            customers.Add(new Customer()
            {
                CustFirstName = "Dwain ",
                CustLastName = "Pickering  ",
                CustEmail = "Dwain@yahoo.com",
                CustPhone = "4382251234"
            });

            customers.Add(new Customer()
            {
                CustFirstName = "Jule ",
                CustLastName = "Bigler  ",
                CustEmail = "jule@gmail.com",
                CustPhone = "4382583641"
            });

            customers.Add(new Customer()
            {
                CustFirstName = "Kristen ",
                CustLastName = "Sergeant  ",
                CustEmail = "kristen@gmail.com",
                CustPhone = "5148951434"
            });

            customers.Add(new Customer()
            {
                CustFirstName = "Lia ",
                CustLastName = "Walla  ",
                CustEmail = "lia@gmail.com",
                CustPhone = "5146214595"
            });

            context.Customers.AddRange(customers);
            base.Seed(context);
            */

            /*
            List<ServerBookingApp.Employee> employees = new List<Employee>();
            employees.Add(new Employee()
            {
                EmpFirstName = "Dayna",
                EmpLastName = "Merrill",
                EmpEmail = "danya@yahoo.com",
                EmpPhone = "4382251233",
                Password= "Danya@123",
                Position = "Hair Stylist",
                Salary = 16.50
            });

            employees.Add(new Employee()
            {
                EmpFirstName = "bobbi",
                EmpLastName = "welk",
                EmpEmail = "admin@admin.com",
                EmpPhone = "5142684532",
                Password = "Admin@123",
                Position = "Hair Stylist",
                Salary = 17.75
            });

            employees.Add(new Employee()
            {
                EmpFirstName = "Stella",
                EmpLastName = "Birdsall",
                EmpEmail = "stella@gmail.com",
                EmpPhone = "5148971235",
                Password = "Stella@123",
                Position = "Hair Stylist",
                Salary = 18.00
            });

            employees.Add(new Employee()
            {
                EmpFirstName = "Reba",
                EmpLastName = "Lemelle",
                EmpEmail = "reba@gmail.com",
                EmpPhone = "5148971239",
                Password = "Reba@123",
                Position = "Hair Stylist",
                Salary = 15.00
            });

            context.Employees.AddRange(employees);
            base.Seed(context);
            */
        }
    }
}
