﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace HostBookingApp
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ServiceHost host = new ServiceHost(typeof(ServerBookingApp.BookingService)))
            {
                host.Open();
                //Console.WriteLine(aDatabase.Connection.ConnectionString);
                Console.WriteLine("Host Starte @" + DateTime.Now.ToShortDateString());
                Console.ReadLine();
            }
            
            
        }
    }
}
