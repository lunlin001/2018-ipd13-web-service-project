﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data.Entity;
using System.Text.RegularExpressions;

namespace ServerBookingApp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BookingService" in both code and config file together.
    public class BookingService : IBookingService
    {
        private BookingDBContext context = new BookingDBContext();
       

        public bool checkAuthentication(string email, string password)        
        {              
            var employee = context.Employees.SingleOrDefault(
                e => e.EmpEmail == email && e.Password == password);
            if (employee != null)
                return true;
            else
                return false;            
        }

        public string getMessage(string name)
        {
            return "Hello " + name;
        }

        public List<Appointment> getTodayAppointments()
        {
            try
            {
                DateTime today = DateTime.Now.Date;
                var appointment = context.Appointments.Include(b => b.Employee)
                    .Include(c => c.Customer).Where(a => a.AptDate == today).ToList();

                return appointment;
            }
            catch(Exception)
            {
                throw;
            }
            
        }

        public void addEmployee(Employee employee)
        {            
                context.Employees.Add(employee);
                context.SaveChanges();           
        }

        public void addCustomer(Customer customer)
        {
            
            context.Customers.Add(customer);
            context.SaveChanges();
        }

        public Employee searchEmployee(string email)
        {
            var employee = context.Employees.Where(e => e.EmpEmail == email).SingleOrDefault();
            return employee;
        }
             

        public Customer searchCustomer(string email)
        {
            var customer = context.Customers.Where(c => c.CustEmail == email).SingleOrDefault();
            return customer;
        }

        public bool checkSearchCus(string lastName)
        {
            var customer = context.Customers.SingleOrDefault(
                c => c.CustLastName == lastName);
            if (customer != null)
                return true;
            else
                return false;
        }

        public void editEmployee(Employee emp)
        {
            var employeeInDB = context.Employees.SingleOrDefault(e => e.id == emp.id);
            employeeInDB.EmpFirstName = emp.EmpFirstName;
            employeeInDB.EmpLastName = emp.EmpLastName;
            employeeInDB.EmpEmail = emp.EmpEmail;
            employeeInDB.Password = emp.Password;
            employeeInDB.Position = emp.Position;
            employeeInDB.Salary = emp.Salary;
            context.SaveChanges();
        }

        public List<Employee> getEmployees()
        {
            var emp = context.Employees.ToList();
            return emp;
        }

        public List<Customer> getCustomers()
        {
            var cust = context.Customers.ToList();
            return cust;
        }

        public List<Customer> searchAllCustomers(string lastName)
        {
            var customers= context.Customers.Where(c => c.CustLastName.Contains(lastName) ).ToList(); //contain acts as a like query in SQL
            return customers;
        }

        public void editCustomer(Customer cust)
        {
            var customerInDB = context.Customers.SingleOrDefault(c => c.id == cust.id);
            customerInDB.CustFirstName = cust.CustFirstName;
            customerInDB.CustLastName = cust.CustLastName;
            customerInDB.CustEmail = cust.CustEmail;
            customerInDB.CustPhone = cust.CustPhone;
        }

        public void bookAppointment(Appointment app)
        {            
            context.Appointments.Add(app);
            context.SaveChanges();
        }
        /********************************** Validation ************************************************/

        public int IsEmailValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return 1;
            }
            catch (FormatException)
            {
                return 0;
            }            
        }

        public int IsNameValid(string name)
        {            
            string pattern = @"^[a-zA-Z]*$";
            if (name.Length < 2 || name.Length > 100 || !Regex.IsMatch(name, pattern))
            {
                return 0;
            }
            return 1;
        }

        public int IsPasswordValid(string pass)
        {
            string pattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}";
            if(!Regex.IsMatch(pass, pattern))
            {
                return 0;
            }
            return 1;
        }

        public int IsSalaryValid(string salary)
        {            
            string pattern = @"[1-9][0-9]?.[0-9]{2}";
            if(!Regex.IsMatch(salary, pattern))
            {
                return 0;
            }
            return 1;
        }

        public int IsPhoneValid(string phone)
        {
            string pattern = @"^[1-9]{1}[0-9]{9}$";           
            if(!Regex.IsMatch(phone, pattern))
            {
                return 0;
            }
            return 1;
        }

        public int IsTimeValid(string time)
        {
            string pattern = @"^[1-9]{1}[0-9]?:[0-9]{2}$";
            if(!Regex.IsMatch(time, pattern))
            {
                return 0;
            }
            return 1;
        }
                
    }
}
