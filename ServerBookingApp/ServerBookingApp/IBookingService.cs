﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;


namespace ServerBookingApp
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBookingService" in both code and config file together.
    [ServiceContract]
    public interface IBookingService
    {
        [OperationContract]
        string getMessage(string name);

        [OperationContract]
        bool checkAuthentication(string email, string password);

        [OperationContract]
        List<Appointment> getTodayAppointments();

        [OperationContract]
        void addEmployee(Employee employee);

        [OperationContract]
        void addCustomer(Customer customer);

        [OperationContract]
        Employee searchEmployee(string email);

        [OperationContract]
        Customer searchCustomer(string lastName);

        [OperationContract]
        bool checkSearchCus(string lastName);

        [OperationContract]
        void editEmployee(Employee emp);

        [OperationContract]
        List<Employee> getEmployees();

        [OperationContract]
        List<Customer> getCustomers();

        [OperationContract]
        List<Customer> searchAllCustomers(string lastName);

        [OperationContract]
        void editCustomer(Customer cust);

        [OperationContract]
        void bookAppointment(Appointment app);

        [OperationContract]
        int IsEmailValid(string emailaddress);

        [OperationContract]
        int IsNameValid(string name);

        [OperationContract]
        int IsPasswordValid(string pass);

        [OperationContract]
        int IsSalaryValid(string salary);

        [OperationContract]
        int IsPhoneValid(string phone);

        [OperationContract]
        int IsTimeValid(string time);        

    }
    
    //[DataContract]
    public class Employee 
    {
        //[DataMember]
        public int id { get; set; }
        public string EmpFirstName { get; set; }
        public string EmpLastName { get; set; }
        public string EmpEmail { get; set; }
        public string Password { get; set; }
        public string EmpPhone { get; set; }
        public string Position { get; set; }
        public double Salary { get; set; }
       
    }

    
    //[DataContract]
    public class Customer 
    {        
        //[DataMember]
        public int id { get; set; }
        public string CustFirstName { get; set; }
        public string CustLastName { get; set; }
        public string CustEmail { get; set; }
        public string CustPhone { get; set; }        
    }

    
    //[DataContract]
    public class Appointment 
    {
        //[DataMember]
        public int id { get; set; }
        public Employee Employee { get; set; }
        public int EmployeeId { get; set; }
        public Customer Customer { get; set; }
        public int CustomerId { get; set; }
        public DateTime AptDate { get; set; }
        public DateTime AptTime { get; set; }

        
    }

    //[DataContract]
    public class BookingDBContext : DbContext
    {
        //[DataMember]
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
    }
}
