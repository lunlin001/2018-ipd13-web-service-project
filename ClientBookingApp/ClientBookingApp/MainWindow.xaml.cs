﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientBookingApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>    

    public partial class MainWindow : Window
    {
        BookingService.BookingServiceClient client = new BookingService.BookingServiceClient();

        public MainWindow()
        {
            InitializeComponent();
            List<BookingService.Appointment> app = new List<BookingService.Appointment>();            
            dgTodayAppointments.ItemsSource = client.getTodayAppointments();
            lblStatus.Content = "Logged In";
        }

        private void newEmp_Click(object sender, RoutedEventArgs e)
        {
            AddEditEmpl addEmp = new AddEditEmpl();
            addEmp.Show();
            this.Hide();
        }

        private void newCust_Click(object sender, RoutedEventArgs e)
        {
            AddEditCust addCus = new AddEditCust();
            addCus.Show();
            this.Hide();
        }

        private void newApp_Click(object sender, RoutedEventArgs e)
        {
            MakeAppointment makeApp = new MakeAppointment();
            makeApp.Show();
            this.Hide();
        }

        private void main_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Hide();
        }        
    }
}
