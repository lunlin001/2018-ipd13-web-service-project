﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientBookingApp
{
    /// <summary>
    /// Interaction logic for AddEditCust.xaml
    /// </summary>
    public partial class AddEditCust : Window
    {
        BookingService.BookingServiceClient client = new BookingService.BookingServiceClient();
        BookingService.Customer searchedCust;

        public AddEditCust()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            BookingService.Customer cus = new BookingService.Customer();

            if (tbCustEmail.Text == null)
            {
                MessageBox.Show("Email is null");
                lblStatus.Content = "Error!";
            }
            else if (tbCustFName.Text == null)
            {
                MessageBox.Show("First name is null");
                lblStatus.Content = "Error!";
            }
            else if (tbCustLName.Text == null)
            {
                MessageBox.Show("Last name is null");
                lblStatus.Content = "Error!";
            }
            else if (tbCustPhone.Text == null)
            {
                MessageBox.Show("Phone number is null");
                lblStatus.Content = "Error!";
            }
            else if (client.IsNameValid(tbCustFName.Text) == 0 || client.IsNameValid(tbCustLName.Text) == 0)
            {
                MessageBox.Show("Either First name or Last name format is not correct. It should only contain" +
                    " alphabetic characters nad between 2-100 characters long.");
                lblStatus.Content = "Error!";
            }
            else if (client.IsEmailValid(tbCustEmail.Text) == 0)
            {
                MessageBox.Show("Email format is not valid.");
                lblStatus.Content = "Error!";
            }
            else if (client.IsPhoneValid(tbCustPhone.Text) == 0)
            {
                MessageBox.Show("Phone number format is not valid. It should contain only numbers and 10 digits long.");
                lblStatus.Content = "Error!";
            }
            else
            {
                cus.CustFirstName = tbCustFName.Text;
                cus.CustLastName = tbCustLName.Text;
                cus.CustPhone = tbCustPhone.Text;
                cus.CustEmail = tbCustEmail.Text;
                client.addCustomer(cus);

                MessageBox.Show("add sucessfully!");
                lblStatus.Content = "Customer Added";

                tbCustFName.Text = string.Empty;
                tbCustLName.Text = string.Empty;
                tbCustEmail.Text = string.Empty;
                tbCustPhone.Text = string.Empty;
            }
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string email = tbInput.Text;
            searchedCust = client.searchCustomer(email);
            if (searchedCust != null)
            {
                tbCustFName.Text = searchedCust.CustFirstName;
                tbCustLName.Text = searchedCust.CustLastName;
                tbCustPhone.Text = searchedCust.CustPhone;
                tbCustEmail.Text = searchedCust.CustEmail;
                lblStatus.Content = "Customer Found";

            }
            else
            {
                MessageBox.Show("There is no customer with this: " + email + " email address");
                lblStatus.Content = "Error!";
            }  
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            searchedCust.CustFirstName = tbCustFName.Text;
            searchedCust.CustFirstName = tbCustLName.Text;
            searchedCust.CustEmail = tbCustLName.Text;
            searchedCust.CustPhone = tbCustPhone.Text;

            if (client.IsNameValid(tbCustFName.Text) == 0 || client.IsNameValid(tbCustLName.Text) == 0)
            {
                MessageBox.Show("Names should be bteween 2-100 characters long and contain only alphabetic charecters.");
            }
            else if (client.IsEmailValid(tbCustEmail.Text) == 0)
            {
                MessageBox.Show("The email format is not valid.");
            }           
            else if (client.IsPhoneValid(tbCustPhone.Text) == 0)
            {
                MessageBox.Show("The phone fielsd is not valid. It is a 10 digit number.");
            }
            else
            {
                client.editCustomer(searchedCust);
                MessageBox.Show("Edited sucessfully!");
                lblStatus.Content = "Employee Edited";
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            tbCustFName.Text = string.Empty;
            tbCustLName.Text = string.Empty;
            tbCustPhone.Text = string.Empty;
            tbCustEmail.Text = string.Empty;
        }

        private void newEmp_Click(object sender, RoutedEventArgs e)
        {
            AddEditEmpl addEmp = new AddEditEmpl();
            addEmp.Show();
            this.Hide();
        }

        private void newCust_Click(object sender, RoutedEventArgs e)
        {
            AddEditCust addCus = new AddEditCust();
            addCus.Show();
            this.Hide();
        }

        private void newApp_Click(object sender, RoutedEventArgs e)
        {
            MakeAppointment makeApp = new MakeAppointment();
            makeApp.Show();
            this.Hide();
        }

        private void main_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Hide();
        }

        
    }
}
