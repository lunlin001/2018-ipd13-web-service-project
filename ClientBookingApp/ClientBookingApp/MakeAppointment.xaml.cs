﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections;
using System.Data;
using System.Globalization;

namespace ClientBookingApp
{
    /// <summary>
    /// Interaction logic for MakeAppointment.xaml
    /// </summary>
    public partial class MakeAppointment : Window
    {
        BookingService.BookingServiceClient client = new BookingService.BookingServiceClient();

        public MakeAppointment()
        {
            InitializeComponent();
            dgCustomers.ItemsSource = client.getCustomers();
            dgEmployees.ItemsSource = client.getEmployees();
        }        

       
        private void dgCustomers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BookingService.Customer cust = dgCustomers.SelectedItem as BookingService.Customer;
            tbCustId.Text = cust.id.ToString();
        }

        private void dgEmployees_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BookingService.Employee emp = dgEmployees.SelectedItem as BookingService.Employee;
            tbEmpId.Text = emp.id.ToString();

        }

        private void tbSearchCust_TextChanged(object sender, TextChangedEventArgs e)
        {
            string searchStr = tbSearchCust.Text;
            dgCustomers.ItemsSource = client.searchAllCustomers(searchStr);
            //lblStatus.Content = "Searching Customer";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BookingService.Appointment app = new BookingService.Appointment();
            if(tbCustId != null || tbEmpId != null || datepicker.SelectedDate != null || tbTime != null)
            {
                try
                {                   
                    app.CustomerId = int.Parse(tbCustId.Text);
                    app.EmployeeId = int.Parse(tbEmpId.Text);
                    app.AptDate = (DateTime)datepicker.SelectedDate;
                    app.AptTime = DateTime.ParseExact(tbTime.Text, "HH:mm", CultureInfo.InvariantCulture);
                    client.bookAppointment(app);
                    MessageBox.Show("Appointment Booked.");
                    lblStatus.Content = "Appointment Booked";
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
            }
            else
            {
                MessageBox.Show("All fields are required to book an appointment.");
            }

            tbCustId.Text = string.Empty;
            tbEmpId.Text = string.Empty;
            datepicker.SelectedDate = null;
            tbTime.Text = string.Empty;
        }

        private void newEmp_Click(object sender, RoutedEventArgs e)
        {
            AddEditEmpl addEmp = new AddEditEmpl();
            addEmp.Show();
            this.Hide();
        }

        private void newCust_Click(object sender, RoutedEventArgs e)
        {
            AddEditCust addCus = new AddEditCust();
            addCus.Show();
            this.Hide();
        }

        private void newApp_Click(object sender, RoutedEventArgs e)
        {
            MakeAppointment makeApp = new MakeAppointment();
            makeApp.Show();
            this.Hide();
        }

        private void main_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Hide();
        }
    }
}
