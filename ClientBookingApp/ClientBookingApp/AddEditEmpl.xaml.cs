﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientBookingApp
{
    /// <summary>
    /// Interaction logic for AddEditEmpl.xaml
    /// </summary>
    public partial class AddEditEmpl : Window
    {
        BookingService.BookingServiceClient client = new BookingService.BookingServiceClient();
        BookingService.Employee searchedEmp;

        public AddEditEmpl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            BookingService.Employee emp = new BookingService.Employee();

            if (client.IsNameValid(tbEmpFName.Text) == 0|| client.IsNameValid(tbEmpLName.Text) == 0)
            {
                MessageBox.Show("Names should be bteween 2-100 characters long and contain only alphabetic charecters.");
            }
            else if (client.IsEmailValid(tbEmpEmail.Text) == 0|| tbEmpEmail.Text == null)
            {
                MessageBox.Show("The email format is not valid.");
            }
            else if (client.IsPasswordValid(tbEmpPass.Password) == 0)
            {
                MessageBox.Show("Password should contain at least one lowercase, one uppercase, one digit or special character and 6 characters long.");
            }
            else if (client.IsPhoneValid(tbEmpPhone.Text) == 0)
            {
                MessageBox.Show("The phone fielsd is not valid. It is a 10 digit number.");
            }
            else if (client.IsSalaryValid(tbEmpSalary.Text) == 0)
            {
                MessageBox.Show("Salary format is not correct. It is a floating point number with two digit precision.");
            }
            else if (client.IsNameValid(tbEmpPosition.Text) == 0)
            {
                MessageBox.Show("Posion field is not valid.");
            }
            else
            {
                try
                {
                    emp.EmpFirstName = tbEmpFName.Text;
                    emp.EmpLastName = tbEmpLName.Text;
                    emp.Password = tbEmpPass.Password.ToString();
                    emp.EmpPhone = tbEmpPhone.Text;
                    emp.Position = tbEmpPosition.Text;
                    emp.Salary = Double.Parse(tbEmpSalary.Text);
                    emp.EmpEmail = tbEmpEmail.Text;
                    client.addEmployee(emp);

                    MessageBox.Show("added sucessfully!");
                    lblStatus.Content = "Employee Added";
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
            }
            
            
        }

        private void Search_Click(object sender, RoutedEventArgs e)
        {
            string email = tbSearch.Text;
            searchedEmp = client.searchEmployee(email);
            tbEmpFName.Text = searchedEmp.EmpFirstName;
            tbEmpLName.Text = searchedEmp.EmpLastName;
            tbEmpPass.Password = searchedEmp.Password;
            tbEmpPhone.Text = searchedEmp.EmpPhone;
            tbEmpPosition.Text = searchedEmp.Position;
            tbEmpSalary.Text = searchedEmp.Salary.ToString();
            tbEmpEmail.Text = searchedEmp.EmpEmail;

            lblStatus.Content = "Employee Found";
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
           
            {
                try
                {
                    searchedEmp.EmpFirstName = tbEmpFName.Text;
                    searchedEmp.EmpLastName = tbEmpLName.Text;
                    searchedEmp.Password = tbEmpPass.Password.ToString();
                    searchedEmp.EmpPhone = tbEmpPhone.Text;
                    searchedEmp.Position = tbEmpPosition.Text;
                    searchedEmp.Salary = Double.Parse(tbEmpSalary.Text);
                    searchedEmp.EmpEmail = tbEmpEmail.Text;
                                       
                   
                    if (client.IsNameValid(tbEmpFName.Text) == 0 || client.IsNameValid(tbEmpLName.Text) == 0)
                    {
                        MessageBox.Show("Names should be bteween 2-100 characters long and contain only alphabetic charecters.");
                    }
                    else if (client.IsEmailValid(tbEmpEmail.Text) == 0)
                    {
                        MessageBox.Show("The email format is not valid.");
                    }

                    else if (client.IsPasswordValid(tbEmpPass.Password) == 0)
                    {
                        MessageBox.Show("Password should contain at least one lowercase, one uppercase, one digit and one special character and 8 characters long.");
                    }
                    else if (client.IsPhoneValid(tbEmpPhone.Text) == 0)
                    {
                        MessageBox.Show("The phone fielsd is not valid. It is a 10 digit number.");
                    }
                    else if (client.IsSalaryValid(tbEmpSalary.Text) == 0)
                    {
                        MessageBox.Show("Salary format is not correct. It is a floating point number with two digit precision.");
                    }
                    else if (client.IsNameValid(tbEmpPosition.Text) == 0)
                    {
                        MessageBox.Show("Position field is not valid.");
                    }
                    else
                    {
                        client.editEmployee(searchedEmp);
                        MessageBox.Show("Edited sucessfully!");
                        lblStatus.Content = "Employee Edited";
                    }

                        
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }           
        }

        private void newEmp_Click(object sender, RoutedEventArgs e)
        {
            AddEditEmpl addEmp = new AddEditEmpl();
            addEmp.Show();
            this.Hide();
        }

        private void newCust_Click(object sender, RoutedEventArgs e)
        {
            AddEditCust addCus = new AddEditCust();
            addCus.Show();
            this.Hide();
        }

        private void newApp_Click(object sender, RoutedEventArgs e)
        {
            MakeAppointment makeApp = new MakeAppointment();
            makeApp.Show();
            this.Hide();
        }

        private void main_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            main.Show();
            this.Hide();
        }

        //public static bool IsPasswordValid(string pass)
        //{
        //    string pattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}";
        //    return Regex.IsMatch(pass, pattern);
        //}
    }

    
}
