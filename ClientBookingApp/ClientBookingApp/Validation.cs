﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ClientBookingApp
{
    class  Validation
    {
        public static bool IsEmailValid(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool IsNameValid(string name)
        {
            string pattern = @"^(?![\s]+$)[a-zA-Z\s]*$";
            if (name.Length < 2 || name.Length > 100 || !Regex.IsMatch(name, pattern))
            {
                return false;
            }
            return true;
        }

        public static bool IsPasswordValid(string pass)
        {
            string pattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}";
            return Regex.IsMatch(pass, pattern);
        }

        public static bool IsSalaryValid(string salary)
        {
            string pattern = @"[1-9][0-9]?.[0-9]{2}";
            if (!Regex.IsMatch(salary, pattern))
            {
                return false;
            }
            return true;
        }

        public static bool IsPhoneValid(string phone)
        {
            string pattern = @"^[1-9]{1}[0-9]{9}$";
            if (!Regex.IsMatch(phone, pattern))
            {
                return false;
            }
            return true;
        }

        public static bool IsTimeValid(string time)
        {
            string pattern = @"^[1-9]{1}[0-9]?:[0-9]{2}$";
            if (!Regex.IsMatch(time, pattern))
            {
                return false;
            }
            return true;
        }
    }
}
